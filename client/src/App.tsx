import { useContext, useState } from "react";
import { EdenClientContext } from "./main";

function App() {
  const [count, setCount] = useState(0);
  const client = useContext(EdenClientContext);
  
  // client.ping.get().then(console.log)

  return (
    <div>
      <div className="text-gray-800">Hello World</div>
      <div className="flex flex-col items-center border-2 rounded-md p-2 m-2 w-fit">
        <div>{count}</div>
        <button onClick={() => setCount(count + 1)}>increment</button>
      </div>
    </div>
  );
}

export default App;
